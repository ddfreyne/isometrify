# encoding: utf-8

require 'chunky_png'
require 'fileutils'

def isometrify(input_image)
  w = input_image.width
  h = input_image.height

  output_image = ChunkyPNG::Image.new(w*2+1, h, ChunkyPNG::Color::TRANSPARENT)

  (0...w).each do |x|
    (0...h).each do |y|
      x2 = x+y
      y2 = (x - y) / 2 + h / 2

      output_image[x2, y2] = input_image[x, y]
    end
  end

  output_image
end

WHITE = ChunkyPNG::Color.rgba(255, 255, 255, 255)
BLACK = ChunkyPNG::Color.rgba(0,   0,   0,   255)

def blend(color, blend_color)
  if ChunkyPNG::Color.fully_transparent?(color)
    color
  else
    ChunkyPNG::Color.interpolate_quick(color, blend_color, 200)
  end
end

def darken(color)
  blend(color, BLACK)
end

def brighten(color)
  blend(color, WHITE)
end

def lower_northwest(x, y, color, w, h)
  if x < w/2
    x2 = x
    y2 = y - (x-w/2)/2 - 1 # -- LOWER NORTH
    c = darken(color)
  else
    x2 = x
    y2 = y
    c = color
  end

  return [x2, y2, c]
end

def raise_northwest(x, y, color, w, h)
  if x < w/2
    x2 = x
    y2 = y + (x-w/2)/2 + (x % 2 == 0 ? 0 : 1) # RAISE NORTH
    c = brighten(color)
  else
    x2 = x
    y2 = y
    c = color
  end

  return [x2, y2, c]
end

def raise_corner(input_image)
  w = input_image.width
  h = input_image.height

  output_image = ChunkyPNG::Image.new(w, h*2, ChunkyPNG::Color::TRANSPARENT)

  (0...w).each do |x|
    (0...h).each do |y|
      x2, y2, c = *lower_northwest(x, y, input_image[x, y], w, h)
      output_image[x2, y2] = c unless ChunkyPNG::Color.fully_transparent?(c)
    end
  end

  output_image
end

def save(image, input_filename, postfix)
  output_dirname = File.dirname(input_filename) + " (#{postfix})"
  FileUtils.mkdir_p(output_dirname)
  output_filename = File.join(output_dirname, File.basename(input_filename))
  image.save(output_filename)
end

ARGV.each do |filename|
  puts "#{filename}…"

  input_image = ChunkyPNG::Image.from_file(filename)

  isometrified_image = isometrify(input_image)
  save(isometrified_image, filename, 'isometric')

  # raised_image = raise_corner(isometrified_image)
  # save(raised_image, filename, 'raised corner')
end
