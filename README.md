# Isometrify

Creates an isometric tile from a 2D texture.

Usage:

	ruby isometrify.rb textures/*.png

Requires [chunky_png](https://github.com/wvanbergen/chunky_png).
